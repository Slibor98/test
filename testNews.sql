-- phpMyAdmin SQL Dump
-- version 4.4.15.7
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Май 24 2017 г., 19:29
-- Версия сервера: 5.6.31
-- Версия PHP: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `testNews`
--

-- --------------------------------------------------------

--
-- Структура таблицы `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL,
  `id_news` int(11) NOT NULL,
  `sender` varchar(255) NOT NULL,
  `comment` text NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `comments`
--

INSERT INTO `comments` (`id`, `id_news`, `sender`, `comment`, `created_at`, `updated_at`) VALUES
(10, 30, 'admin', 'супер', 1495641121, 1495641121),
(11, 30, 'admin', 'класс', 1495641127, 1495641127),
(12, 31, 'admin', 'ммм', 1495642372, 1495642372);

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1495196906),
('m130524_201442_init', 1495196926),
('m170519_141910_add_user', 1495204817),
('m170519_144427_create_table_tags', 1495205380),
('m170519_163428_create_table_news', 1495212127),
('m170519_214049_add_column_date_for_news', 1495230148),
('m170520_130450_create_table_comments', 1495285614),
('m170521_090201_create_table_tags_info', 1495357595),
('m170521_135619_add_column_tag_for_news', 1495375212);

-- --------------------------------------------------------

--
-- Структура таблицы `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL,
  `id_tags` int(11) NOT NULL,
  `header` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `photo_src` varchar(255) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `date` date DEFAULT NULL,
  `id_tag` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `news`
--

INSERT INTO `news` (`id`, `id_tags`, `header`, `text`, `photo_src`, `created_at`, `updated_at`, `date`, `id_tag`) VALUES
(30, 1, 'Как борисовчанин стал своим в бельгийском шоу-бизнесе', '<p><strong><span style="color:rgb(102, 102, 102)">В Брюсселе 26 градусов выше нуля, все это на фоне интенсивного солнца &mdash; для города, в котором дождит плюс-минус постоянно, поистине редкий день. В закоулке Гранд-Пляса живет тихонький ресторан, которым владеет белорус. Возле входа сидит еще один. Из-под столика виднеются высокие &laquo;конверсы&raquo; коричневой масти. Их хозяин родился в Узде, потом переехал с родителями в Борисов, а затем волею судеб и девяностых годов оказался в бельгийском Руселаре. Его зовут Олег Джаггер.</span></strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p style="margin-left:auto !important; margin-right:auto !important">Вообще, паспортная фамилия этого человека &mdash; Минаков. Просто лет сорок назад Олегу на глаза попался журнал &laquo;Крокодил&raquo;. На одном из разворотов была статья, в которой высмеивалась западная рок-культура. На фотографии Мик Джаггер правой рукой держится за голову, левой &mdash; за микрофон, рот широко открыт.</p>\r\n\r\n<p style="margin-left:auto !important; margin-right:auto !important">Олег взял журнал в школу. Учительница что-то объясняла, а Минаков, спрятавшись на задней парте, хвастался фоткой перед друзьями. Конечно, ребят засекли. Учительница выхватила журнал:&nbsp;<em>&laquo;Это что за обезьяна?&raquo;</em>&nbsp;&mdash;&nbsp;<em>&laquo;Какая же это обезьяна? Это же Джаггер&raquo;.</em>&nbsp;&mdash;&nbsp;<em>&laquo;Сам ты Джаггер &mdash; такая же обезьяна&raquo;.</em>&nbsp;Так у Олега появился творческий псевдоним.</p>\r\n\r\n<p style="margin-left:auto !important; margin-right:auto !important"><img alt="" src="/backend/web/assets/e073fd7/upload/images/39d1e351203cb33f6f4498eeccdee1e3.jpeg" style="height:733px; width:1100px" /></p>\r\n\r\n<p style="margin-left:auto !important; margin-right:auto !important">&mdash; После увольнения из армии я сразу поступил на работу в Белорусскую государственную филармонию. В итоге провел там период с 1988 по 1990 год. Правда, ровно посредине, в 1989-м, сделал первую роковую программу. Такой микс из Van Halen, AC/DC плюс немножко Whitesnake. Репетиционная точка у нас была в школе милиции. Надо было ехать за город. Там мы и сдавали программу комиссии из филармонии.</p>\r\n\r\n<p style="margin-left:auto !important; margin-right:auto !important">Собрался полный зал. Курсантики с подружками пришли. А мы отстегнули хороший такой рок. Ребята возбудились, покидали кителя и стали рвать на себе рубашки, орали, плясали. Глава комиссии Виктор Вуячич резюмировал:&nbsp;<em>&laquo;Если сделали это с милицией, нельзя их выпускать в люди. Это ж дикость! Меняйте программу!&raquo;</em>&nbsp;А я уперся:&nbsp;<em>&laquo;Не буду ничего менять!&raquo;</em>&nbsp;Рок был неплохой, но на английском языке. Не для того времени формат.</p>\r\n\r\n<p style="margin-left:auto !important; margin-right:auto !important"><img alt="" src="/backend/web/assets/e073fd7/upload/images/ebc92cf8c37d3cb8baedac34eee68b32.jpeg" style="height:733px; width:1100px" /></p>\r\n\r\n<p style="margin-left:auto !important; margin-right:auto !important">Все у меня ровно и хорошо. И вообще, после 50 ты уже больше паришься по здоровью. Честно, ходишь уже по дому, как Оззи Озборн, шаркаешь тапками по паркету, но потом на сцене выдаешь такую цаплю, что никто не верит.</p>\r\n\r\n<p style="margin-left:auto !important; margin-right:auto !important">В Брюсселе по-прежнему 26. Скоро Джаггер сядет на поезд и спокойно поедет в почти родной уже Руселаре.</p>\r\n\r\n<p>&nbsp;</p>\r\n', 'news_photo.jpeg', 1495641063, 1495641063, '2017-05-24', 4),
(31, 1, 'Премьера нового «Gastrofest. Крафт». Что дают за 20 рублей', '<p style="margin-left:auto !important; margin-right:auto !important">С четверга в Минске начинается очередной Gastrofest. Тема нового фестиваля, который продлится до 14 июня, &mdash; крафтовое пиво и соответствующие напитку закуски. 20 пивных заведений Минска приготовят уникальные сеты. Представляем их на фото. Не попробуете &mdash; так полюбуйтесь.</p>\r\n\r\n<p style="margin-left:auto !important; margin-right:auto !important">Гастрофест &mdash; это серия гастрономических фестивалей. У них разные темы, но одна механика. 20 топовых в своем сегменте заведений предлагают лучшие сеты по фиксированной цене в определенный отрезок времени. Стоимость сета &mdash; 20 рублей. Пива на картинках нет, но оно будет на вашем столе за 5 рублей &mdash;&nbsp;уникальные сорта от поставщика в каждом заведении.</p>\r\n\r\n<p style="margin-left:auto !important; margin-right:auto !important">Ну и традиционно при расчете карточкой &laquo;Альфа-Банка&raquo; за любой фестивальный сет &mdash; приятные подарки.</p>\r\n\r\n<p style="margin-left:auto !important; margin-right:auto !important"><img alt="" src="/backend/web/assets/e073fd7/upload/images/ec2f0a523239778dd93c209ce7b1523f.jpeg" style="height:400px; width:600px" /><img alt="" src="/backend/web/assets/e073fd7/upload/images/0c9a48bef658c4eeccb5cdf5994fad21.jpeg" style="height:400px; width:600px" /></p>\r\n\r\n<p style="margin-left:auto !important; margin-right:auto !important">Гастрофест &mdash; это серия гастрономических фестивалей. У них разные темы, но одна механика. 20 топовых в своем сегменте заведений предлагают лучшие сеты по фиксированной цене в определенный отрезок времени. Стоимость сета &mdash; 20 рублей. Пива на картинках нет, но оно будет на вашем столе за 5 рублей &mdash;&nbsp;уникальные сорта от поставщика в каждом заведении.</p>\r\n', 'news_photo.jpeg', 1495641257, 1495642533, '2017-05-24', 8),
(32, 2, 'Ищем легенду в деревенских дворах, за дверями гараже', '<p><span style="color:rgb(102, 102, 102)">Обычно при покупке подержанного автомобиля пристально разглядывают царапинки и вздутия краски, &laquo;простукивают&raquo; кузов толщиномером, изучают сервисную книжку... Но в случае с проектом &laquo;Возрождая легенды&raquo; ориентировались на совершенно иные критерии. А двери откроются? Когда заводился последний раз? Прошлой осенью? Так еще недавно ездил! Есть запасные детали? Шутка ли! Речь идет о машинах, выпущенных более полувека назад, &mdash; 403-м и 407-м &laquo;Москвичах&raquo;, ГАЗ-21 и др. В пул попали семь &laquo;старичков&raquo;. Некоторые мы нашли во дворах деревенских домов. Другие не выезжали так давно, что двери гаражей приходилось буквально откапывать. Но было несколько очень интересных экземпляров.&nbsp;Впрочем, обо всем по порядку: в первой публикации рассказываем о том, как мы искали кандидатов для восстановления &laquo;легенды советского автопрома&raquo;.<img alt="" src="/backend/web/assets/e073fd7/upload/images/8ffd3335341bdc5c5409de11353678de.jpeg" style="height:534px; width:800px" /></span></p>\r\n\r\n<p style="margin-left:auto !important; margin-right:auto !important">Чтобы рассмотреть все варианты, пришлось объездить Минск, окрестности, а также другие города и поселки. Этот&nbsp;<a href="http://ab.onliner.by/car/3381961" style="cursor: pointer; color: rgb(26, 109, 187); text-decoration-line: none; transition: color 0.2s ease;">&laquo;Москвич-403&raquo;</a>&nbsp;мы нашли в ближайшем пригороде. Модель во&nbsp;многом &laquo;списали&raquo; с&nbsp;Opel Kadett. Как у&nbsp;многих советских машин, существовало несколько модификаций&nbsp;&mdash; для такси, экспортный вариант, &laquo;южный&raquo;.</p>\r\n\r\n<p style="margin-left:auto !important; margin-right:auto !important">Как-то сразу повелось, что всем автомобилям мы&nbsp;с ходу давали кличку. Уж&nbsp;очень индивидуальным казался каждый экземпляр из&nbsp;советского прошлого. К&nbsp;этой машине за&nbsp;ее&nbsp;небесно-голубой цвет сразу приклеилось прозвище &laquo;Василек&raquo;.</p>\r\n\r\n<p style="margin-left:auto !important; margin-right:auto !important"><img alt="" src="/backend/web/assets/e073fd7/upload/images/e6507e508e7ec8af60299ecdadf1ca87.jpeg" style="height:534px; width:800px" /></p>\r\n\r\n<p style="margin-left:auto !important; margin-right:auto !important">Это круто</p>\r\n', 'news_photo.jpeg', 1495641393, 1495643109, '2017-05-24', 2),
(33, 3, 'Разрушаем мифы. Краш-тест нового и старого Nokia 3310', '<p>Скажем прямо &mdash; новый&nbsp;<a href="https://catalog.onliner.by/mobile/nokia/nokia3310" style="cursor: pointer; color: rgb(26, 109, 187); text-decoration-line: none; transition: color 0.2s ease; font-family: &quot;Open Sans&quot;, Arial, Helvetica, Verdana, sans-serif; font-size: 16px;">3310</a>&nbsp;не производит впечатление крепыша. Да, при попытке открыть крышку он сломал нам карту microSD и едва не повредил пару отверток, но тонкий пластик и большой дисплей не внушали доверия. Поэтому первые краш-тесты мы сделали шуточными. Телефон выглядит как игрушка, поэтому и начать можно с ненастоящих испытаний.</p>\r\n\r\n<p><img alt="" src="/backend/web/assets/e073fd7/upload/images/5fe8811a5a765dbcb0ed3ed20fa44d77.jpeg" style="height:766px; width:1400px" />Любимый игрушечный автомобильчик Дениса Логуновского без проблем проехался по двум аппаратам, оставив разве что пыльные следы от шин. Пенопласт чуть не сдуло, но мы его догнали и на всякий случай ударили им оба Nokia 3310.</p>\r\n\r\n<p><img alt="" src="/backend/web/assets/e073fd7/upload/images/d8f0c552d7a758f7c4777734d50dd43b.jpeg" style="height:733px; width:1100px" />Жаль, выяснять отношения между классической моделью и кирпичом бесполезно. Телефон не выдержал всего-то мягкой резины, куда уж ему тягаться с чем-то более серьезным и керамическим. Зато у нас остался новичок, пусть он отдувается за честь прадеда.</p>\r\n\r\n<p>&nbsp;</p>\r\n', 'news_photo.jpeg', 1495641525, 1495642606, '2017-05-24', 7),
(34, 1, 'Будем открывать магазин сами, потому что белорусский ретейл вообще не чешется', '<p>Максим Кондратенко&nbsp;&mdash;&nbsp;директор ООО &laquo;Мегавента&raquo;. Бизнесом занимается более 10 лет. Импортер и дистрибьютор механических 3D-пазлов Ugears в Беларуси.</p>\r\n\r\n<p>Мы долгое время занимались торговлей бытовой техникой, но сегодня наблюдаю, как разваливаются привычные, отработанные десятилетиями схемы работы. Вместо сотен продавцов одинаковой продукции на рынке появляются очевидные лидеры, которые просто &laquo;съедают&raquo; всех, кто слабее. Сопротивляться, барахтаться, конечно, можно, но лично для меня очевидно, что это всего лишь отсрочит агонию. Реальными альтернативами классического &laquo;купи-продай&raquo;, который длился 20 лет, может стать занятость в сфере услуг, производство или клиентоориентированный импорт.</p>\r\n\r\n<p style="margin-left:auto !important; margin-right:auto !important">Понятно, что производство &mdash; это далеко не для всех: как правило, это долго, дорого и технически сложно. Сфера услуг лично для меня малопривлекательна. Поэтому наиболее перспективными в моих глазах стали поставки в страну какой-то необычной, не имеющей аналогов продукции. В идеале &mdash; поиск ниши, в которой пока конкуренция близка к нулю.</p>\r\n\r\n<p style="margin-left:auto !important; margin-right:auto !important">Мы для себя нашли такую нишу. Занялись эксклюзивными поставками в Беларусь необычных деревянных 3D-пазлов &mdash; своеобразных игрушек для взрослых. Продукт, с одной стороны, может прекрасно разнообразить несколько вечеров человека, любящего мастерить, а с другой &mdash; является отличной декорацией для интерьера.</p>\r\n\r\n<p style="margin-left:auto !important; margin-right:auto !important">Расчет был точный: мы договорились с украинским заводом, который организовал этот стартап, об&nbsp;официальном представительстве в Беларуси, начали создавать дилерскую сеть, но практически сразу столкнулись с совершенно отвратительными реалиями, которые царят в белорусском ретейле.</p>\r\n\r\n<p style="margin-left:auto !important; margin-right:auto !important">Если сервис в электронной торговле в нашей стране за последние годы приблизился к европейским стандартам, то розница до сих пор нередко &laquo;плавает&raquo; на уровне сельпо доперестроечных времен или палаточных городков на рынках из лихих 90-х прошлого века.</p>\r\n\r\n<p style="margin-left:auto !important; margin-right:auto !important">Нам это стало особенно отчетливо видно именно потому, что продажа сувениров &mdash; это достаточно специфический вид торговли, в котором продавец должен быть &laquo;включен&raquo; в процесс общения с покупателем практически все время, что на данный момент практически невозможно, если говорить о белорусском маркетинге.</p>\r\n\r\n<p style="margin-left:auto !important; margin-right:auto !important"><img alt="" src="/backend/web/assets/e073fd7/upload/images/5c57b267df221bf12ff76dcfb82fd33b.jpeg" style="height:534px; width:800px" /></p>\r\n\r\n<p style="margin-left:auto !important; margin-right:auto !important">Расчет был точный: мы договорились с украинским заводом, который организовал этот стартап, об&nbsp;официальном представительстве в Беларуси, начали создавать дилерскую сеть, но практически сразу столкнулись с совершенно отвратительными реалиями, которые царят в белорусском ретейле.</p>\r\n', 'news_photo.jpeg', 1495641634, 1495642634, '2017-05-24', 7),
(35, 1, 'Мы меньше читаем, но будем становиться все умнее… Не вижу противоречия!»', '<p>Александр Ханин&nbsp;&mdash;&nbsp;один из создателей международного развлекательного проекта &laquo;МозгоБойня&raquo;. Родился в Минске, окончил факультет социальных наук ЕГУ в Вильнюсе. Работал журналистом, футбольным судьей, бизнес-тренером. В 2012 году с Катериной Максимовой провел первую &laquo;МозгоБойню&raquo; для минчан. Пару недель назад проекту исполнилось 5 лет.</p>\r\n\r\n<p>Вы наверняка слышали, что современная молодежь стала меньше читать, что в большинстве своем она черпает информацию из интернета в формате дайджестов, рейтингов и &laquo;интересных фактов&raquo;. Все это действительно имеет место. Признаюсь, сам я прикасаюсь к книгам все реже, а дочитываю до конца, наверное, одну из пяти. Но! Во всем этом я не вижу абсолютно ничего плохого. Мы просто вступили в эру с новым форматом получения знаний и обмена ими. Заметьте, несмотря на &laquo;неначитанность&raquo; молодежи, среди ее представителей все большее распространение получают интеллектуальные игры. Согласитесь, происходит это неспроста.</p>\r\n\r\n<p style="margin-left:auto !important; margin-right:auto !important">Мы создали &laquo;МозгоБойню&raquo; пять лет назад, подсмотрев концепцию игры у своих литовских товарищей. Скажу честно и сразу: это не наше ноу-хау (мы и не претендуем), другое дело, что мы потратили очень много сил на полировку концепции. Самое главное, что мы уловили &mdash; и это, мне кажется, вас немного удивит, &mdash; что наша &laquo;МозгоБойня&raquo; &mdash; это игра не про интеллект.</p>\r\n\r\n<p style="margin-left:auto !important; margin-right:auto !important">Вы спросите, как так? Поинтересуйтесь у десяти человек из тех, кто пришел к нам, что они делали на протяжении игры, и девять из них ответят: нам задавали вопросы, а мы отвечали&hellip; Некоторые, безусловно, приходят, чтобы сражаться, занимать первые места, упражняться для развития своих интеллектуальных способностей и эрудиции. Но большинство все-таки стремится просто хорошо провести время. И секрет нашего успеха заключается как раз в том, что мы не стремимся стать &laquo;элитными&raquo; и &laquo;интеллектуальными&raquo;. Наша задача &mdash; развлечение, и я сравнил бы это, например, с походом в кино. Только при этом можно еще и пивасик попить и с друзьями пообщаться&hellip;</p>\r\n\r\n<p style="margin-left:auto !important; margin-right:auto !important">Я говорю это к тому, что растет и ширится категория людей, которые развиваются не от прочтения уймы книг, а в процессе общения, живого обмена информацией. И эти молодые люди уже привыкли к комфорту (а зачем от него отказываться?). По сути, им сказали, где, когда и что произойдет. С книгами все сложнее: надо подумать, какую книгу выбрать, когда найти время на ее прочтение, стоит ли дочитывать&hellip; А здесь всего две опции: идти или не идти. Четыре тысячи человек в Минске выбирают &laquo;да&raquo;, остальные два миллиона &mdash; &laquo;нет&raquo;. И мы видим в этом большой задел для развития.</p>\r\n\r\n<p style="margin-left:auto !important; margin-right:auto !important">И поверьте, эти люди развиваются стремительно, хоть и без видимых для классического образования напрягов. Их кругозор расширяется от игры к игре, их мозги напрягаются и привыкают к регулярным нагрузкам. При этом проекты, подобные нашему, &mdash; это вроде как всего лишь повод встретиться. При этом не нужно сидеть друг напротив друга и спрашивать &laquo;Что у тебя нового?&raquo;. Такие игры помогают вновь налаживать старые связи.</p>\r\n\r\n<p style="margin-left:auto !important; margin-right:auto !important">Мы никогда не думали, что идея интеллектуальной игры может стать коммерчески выгодным проектом. Поначалу это было скорее хобби, которое приносит чуточку денег. Это захватывало настолько, что в какой-то момент стало понятно: или придется завязывать, или мы отдаемся процессу целиком, не отвлекаясь ни на что иное</p>\r\n\r\n<p style="margin-left:auto !important; margin-right:auto !important"><img alt="" src="/backend/web/assets/e073fd7/upload/images/19220481998c05b9ee6a5c4611037839.jpeg" style="height:533px; width:800px" /></p>\r\n\r\n<p style="margin-left:auto !important; margin-right:auto !important">При этом проекты, подобные нашему, &mdash; это вроде как всего лишь повод встретиться. При этом не нужно сидеть друг напротив друга и спрашивать &laquo;Что у тебя нового?&raquo;. Такие игры помогают вновь налаживать старые связи.</p>\r\n', 'news_photo.jpeg', 1495641709, 1495642667, '2017-05-24', 7),
(36, 4, 'Все по 2,40, но есть скидки. Что и за сколько можно купить в магазинах одной цены в Минске', '<p style="margin-left:auto !important; margin-right:0px !important">Прямо по соседству с богатым ночным клубом Dozari открылся более скромный по предназначению, но дорогой сердцу любого ценителя маркетинговых околичностей магазин. Здесь все продают по фиксированной цене &mdash; все три тысячи наименований, если верить праздничной вывеске. 2 рубля 40 копеек &mdash; потолок, в который утыкаются цены. Побывали в магазине и узнали, как идут дела.</p>\r\n\r\n<p style="margin-left:auto !important; margin-right:0px !important">На самом деле, магазины одной цены нельзя назвать ноу-хау для Беларуси. Вот этот, например, входит в крупную сеть Euroshop, которая работает по всей стране: в Минске таких магазинов уже семь, а если суммировать их с представительствами в других городах, то получится двузначная цифра, приближающаяся к двадцати.</p>\r\n\r\n<p style="margin-left:auto !important; margin-right:0px !important"><img alt="" src="/backend/web/assets/e073fd7/upload/images/eb91f23732a9ddbdd1ac29da5029295f.jpeg" style="height:400px; width:800px" /></p>\r\n\r\n<p style="margin-left:auto !important; margin-right:0px !important">Этот двухэтажный магазин примечателен тем, что находится прямо на центральном проспекте, а на полу еще виднеется напоминанием о прошлом постояльце надпись Ziko.</p>\r\n\r\n<p style="margin-left:auto !important; margin-right:0px !important"><img alt="" src="/backend/web/assets/e073fd7/upload/images/270865599d65ea24985d38093453e5d2.jpeg" style="height:533px; width:800px" /></p>\r\n\r\n<p style="margin-left:auto !important; margin-right:0px !important">Есть еще товары скидочные &mdash; те, что не по 2,40, а еще дешевле. Они стоят отдельно и помечены специально, чтобы глаз мог схватиться за выгодное предложение среди прочих выгодных предложений.</p>\r\n\r\n<p style="margin-left:auto !important; margin-right:0px !important"><img alt="" src="/backend/web/assets/e073fd7/upload/images/95bab5ee6b1195e7e5f7ece6f828168e.jpeg" style="height:533px; width:800px" /></p>\r\n\r\n<p style="margin-left:auto !important; margin-right:0px !important">Больше товаров на официальном сайте</p>\r\n', 'news_photo.jpeg', 1495641851, 1495642719, '2017-05-24', 6),
(37, 4, 'В МАЗ, который вот уже несколько лет показывает убытки, планируют вложить $500 миллионов', '<p style="margin-left:auto !important; margin-right:0px !important">Об этом заявил вице-премьер Владимир Семашко. По его словам, в развитие предприятия нужно вкладывать серьезные деньги, производить новые мосты, оси и кабины. Он отметил, что сейчас решается вопрос с источниками финансирования. Между тем, автомобильный завод вот уже который год показывает убытки. Стоит ли вкладывать в него деньги, Onliner.by спросил у экономиста.</p>\r\n\r\n<p style="margin-left:auto !important; margin-right:0px !important"><em>&mdash; В развитие МАЗа надо серьезно вкладывать средства &mdash; в новую кабину, новые мосты делать, новые оси. Есть инвестиционные программы, сейчас мы решаем проблемы с источниками финансирования, &mdash; будем развивать предприятие,</em>&nbsp;&mdash;&nbsp;<a href="http://www.belta.by/economics/view/v-razvitie-maza-planiruetsja-vlozhit-okolo-500-mln-semashko-249009-2017/#top" style="cursor: pointer; color: rgb(26, 109, 187); text-decoration-line: none; transition: color 0.2s ease;" target="_blank">цитирует</a>&nbsp;Семашко БЕЛТА. По словам вице-премьера, сумма инвестиций, о которой идет речь, может составить около полумиллиарда долларов.</p>\r\n\r\n<p style="margin-left:auto !important; margin-right:0px !important">&nbsp;</p>\r\n\r\n<p style="margin-left:auto !important; margin-right:0px !important">&nbsp;</p>\r\n\r\n<p style="margin-left:auto !important; margin-right:0px !important">х финансирования.</p>\r\n\r\n<p style="margin-left:auto !important; margin-right:0px !important">Семашко заявил, что МАЗ переживает не самый лучший период.</p>\r\n\r\n<p style="margin-left:auto !important; margin-right:0px !important"><em>&mdash; Есть проблемы, в том числе в объемах: если еще года три назад предприятие делало 24 тыс. автомобилей, в этом 6,7 тыс. делают и могут не выйти даже на 11 тыс. по итогам года,</em>&nbsp;&mdash; сказал он. Также вице-премьер подчеркнул, что страна заинтересована в приходе инвесторов на белорусские промпредприятия.</p>\r\n\r\n<p>МАЗ входил в топ-10 самых убыточных предприятий страны</p>\r\n\r\n<p style="margin-left:auto !important; margin-right:0px !important">По последним данным (за второй квартал 2016 года), рейтинг аутсайдеров выглядит&nbsp;<a href="https://people.onliner.by/2016/06/30/oao-2" style="cursor: pointer; color: rgb(26, 109, 187); text-decoration-line: none; transition: color 0.2s ease;" target="_blank">так</a>&nbsp;(млн деноминированных рублей убытков):</p>\r\n\r\n<p>БМЗ &mdash; управляющая компания холдинга БМК &mdash; 221,915;</p>\r\n\r\n<p>&laquo;Гомсельмаш&raquo; &mdash; 99,245;</p>\r\n\r\n<p>ГЗЛиН &mdash; 52,506;</p>\r\n\r\n<p>&laquo;Белшина&raquo; &mdash; 44,679;</p>\r\n\r\n<p>МАЗ &mdash; 29,259;</p>\r\n\r\n<p>&laquo;Красносельскстройматериалы&raquo; &mdash; 19,06;</p>\r\n\r\n<p>&laquo;Кричевцементношифер&raquo; &mdash; 14,721;</p>\r\n\r\n<p>Авиакомпания &laquo;Белавиа&raquo; &mdash; 14,636;</p>\r\n\r\n<p>МТЗ &mdash; 14,278;</p>\r\n\r\n<p>Светлогорский ЦКК &mdash; 10,307.</p>\r\n\r\n<p style="margin-left:auto !important; margin-right:0px !important">Всего же, по данным за второй квартал 2016 года, 747 ОАО Беларуси из 2235 находились в убытке, 56 вышли в ноль.</p>\r\n\r\n<p style="margin-left:auto !important; margin-right:0px !important"><img alt="" src="/backend/web/assets/e073fd7/upload/images/224a30d0c5ab35e69249d541bdcdafa8.jpeg" style="height:683px; width:1024px" /></p>\r\n\r\n<p style="margin-left:auto !important; margin-right:0px !important"><em>Непонятно, есть ли она в бизнес-плане, о котором говорил вице-премьер. Непонятно, предусматривает ли бизнес-план стресс-сценарии: что будет, если нефть упадет в цене, что будет, если в России снова просядет рынок, и так далее. Судя по ситуации с деревообработкой и цементной отраслью, таких сценариев не было.</em></p>\r\n\r\n<p style="margin-left:auto !important; margin-right:0px !important"><em>Помните историю с деревообработкой? Тогда бизнес-планы были просты: все модернизируем и начнем продавать продукцию в Россию. Но когда модернизация закончилась, оказалось, что в России просел рынок и появились свои аналогичные предприятия. Наши чиновники стали говорить: &laquo;А кто ж знал, что в России появятся свои производства?&raquo; А ведь нормальный бизнес-план должен все эти нюансы предусматривать. Предусматривает ли новый план по модернизации МАЗа стресс-варианты и есть ли в нем выходы из ситуации, непонятно.</em></p>\r\n\r\n<p style="margin-left:auto !important; margin-right:0px !important"><em>Прозвучал такой советский термин &laquo;освоить&raquo;. С какой рентабельностью эти деньги будут инвестированы, когда окупятся и окупятся ли вообще, про это не было сказано ни одного слова.</em><em>&nbsp;Это ярчайшая иллюстрация тех проблем, которые испытывает наш госсектор в экономике.</em></p>\r\n\r\n<p style="margin-left:auto !important; margin-right:0px !important"><em><img alt="" src="/backend/web/assets/e073fd7/upload/images/ab6044c046a36d07a11b3327e618b6af.jpeg" style="height:683px; width:1024px" /></em></p>\r\n\r\n<p style="margin-left:auto !important; margin-right:0px !important">&nbsp;</p>\r\n\r\n<p style="margin-left:auto !important; margin-right:0px !important">Между тем в последние годы дела у Минского автомобильного завода идут не очень. За прошлый год чистый убыток предприятия&nbsp;<a href="http://maz.by/media/12128/%D0%BE%D1%82%D1%87%D0%B5%D1%82-%D0%BE-%D0%BF%D1%80%D0%B8%D0%B1%D1%8B%D0%BB%D1%8F%D1%85-%D0%B8-%D1%83%D0%B1%D1%8B%D1%82%D0%BA%D0%B0%D1%85-%D0%B7%D0%B0-%D1%8F%D0%BD%D0%B2%D0%B0%D1%80%D1%8C-%D0%B4%D0%B5%D0%BA%D0%B0%D0%B1%D1%80%D1%8C-2016-%D0%B3%D0%BE%D0%B4%D0%B0.pdf" style="color: rgb(26, 109, 187); cursor: pointer; text-decoration-line: none; transition: color 0.2s ease;" target="_blank">составил</a>&nbsp;81,656 миллиона рублей, в 2015 году &mdash; 96,731 миллиона, в 2014-м &mdash;&nbsp;156,304 миллиона деноминированных рублей.</p>\r\n', 'news_photo.jpeg', 1495641936, 1495642751, '2017-05-24', 2),
(38, 1, '«Купаться здесь? Нет, спасибо», «Не хватает шаурменных и горячей кукурузы». ', '<p style="margin-left:auto !important; margin-right:0px !important"><span style="color:rgb(102, 102, 102)">Минские пляжи пока не кишат разгоряченными телами, купальники красавиц не сливаются в цветовую симфонию, вода у берега относительно чистая. Купальный сезон в этом году запоздал: его можно считать открытым лишь с минувших выходных. И пусть песчаные берега в столице заменяют заросшие травой спуски, а вместо морского прибоя доносится музыка из чьих-то динамиков, но ведь отдыхать-то хочется! Пляжи в черте города &mdash; пока самый доступный способ полечить нервишки у воды. Готовы ли они встретить отдыхающих достойно? Что думают о них минчане? Onliner.by посетил самые популярные пляжи столицы: на Комсомольском озере, Цнянском водохранилище и водохранилище Дрозды.</span></p>\r\n\r\n<p style="margin-left:auto !important; margin-right:0px !important"><span style="color:rgb(102, 102, 102)"><img alt="" src="/backend/web/assets/e073fd7/upload/images/6d346c396a2292944901d98485011a2b.jpeg" style="height:533px; width:800px" /></span></p>\r\n\r\n<p style="margin-left:auto !important; margin-right:auto !important">Несмотря на все это, минчане обязательно должны быть чем-то недовольны! Спрашиваем. Григорий и Диана сворачивают по тропинке в сторону выхода:</p>\r\n\r\n<p style="margin-left:auto !important; margin-right:auto !important"><em>&mdash; Купаться здесь? Нет, спасибо. Я не привык на таких пляжах купаться. На каких привык? На морских. Что если нет возможности поехать на море? Сочувствую.</em></p>\r\n\r\n<p style="margin-left:auto !important; margin-right:auto !important"><em><img alt="" src="/backend/web/assets/e073fd7/upload/images/e2fadb0cfb898859e510c6f2c52e09f3.jpeg" style="height:533px; width:800px" />&mdash; Летом тут вообще беда будет, как набегут люди. Хочется, чтобы их как-то поменьше было. Лучше за город уехать,</em>&nbsp;&mdash; молодой человек категоричен, но не прочь поразмышлять. &mdash;&nbsp;<em>Почему бы не натянуть лебедку, чтобы люди могли кататься? Развлекух тут вообще мало. Чем заняться отдыхающим? Сравниваю с пляжами Москвы. Туда приезжаешь как на море: батуты, лебедки, музыка, танцпол, песочек белый. А у нас &mdash; ни о чем. Что на пляже сделано? Два мангала поставили? Хотя если сравнить не с чем, то вполне достойный пляж.</em></p>\r\n\r\n<p style="margin-left:auto !important; margin-right:auto !important"><em><img alt="" src="/backend/web/assets/e073fd7/upload/images/794d5466fbcff2e9b7633cff4dc36485.jpeg" style="height:533px; width:800px" /></em></p>\r\n\r\n<p style="margin-left:auto !important; margin-right:auto !important">&nbsp;</p>\r\n\r\n<p style="margin-left:auto !important; margin-right:auto !important">Вот так жестко молодой человек перечеркнул все старания городских служб. А между тем количество довольных превышает количество придирчивых.</p>\r\n\r\n<p style="margin-left:auto !important; margin-right:auto !important">Юля и Женя учатся в колледже. Девушек охотно отпускают с практики, поэтому они без зазрения совести нежатся на солнышке:</p>\r\n\r\n<p style="margin-left:auto !important; margin-right:auto !important"><em>&mdash; Мы здесь постоянно. Летом людей так много, что пройти негде. Вода из-за них не очень чистая, на пляже мусорят. А еще тут часто выпивают. Нам они не очень мешают, но, бывает, подходят к нам, что-то говорят &mdash; неприятно. Но милиция часто ходит, проверяет. А как еще избавиться от пьяных? Единственный вариант &mdash; ввести сухой закон.</em></p>\r\n\r\n<p style="margin-left:auto !important; margin-right:auto !important"><em>А насчет инфраструктуры&hellip; Раньше вообще не было ни скамеек, ни беседок. Сейчас все есть, даже магазин открыли. А вообще, главное &mdash; это природа и то, что она близко к дому.</em></p>\r\n', 'news_photo.jpeg', 1495642172, 1495642793, '2017-05-24', 3),
(39, 1, '«Минчане к нам пачками едут на ПМЖ»', '<p>Более полувека назад Свислочь называли объектом стратегического назначения. Продукцию местного завода использовали при строительстве ракет и самолетов, а директора предприятия регулярно бывали на совещаниях в Кремле. Как сложилась судьба поселка в Пуховичском районе после развала Союза и почему население тут только растет, читайте в новом выпуске проекта Onliner.by&nbsp;<a href="https://people.onliner.by/tag/monogorod" style="cursor: pointer; color: rgb(26, 109, 187); text-decoration-line: none; transition: color 0.2s ease;">&laquo;Моногород&raquo;</a>.</p>\r\n\r\n<p style="margin-left:auto !important; margin-right:auto !important">Цветущего зеленого поселка сегодня могло и не быть, если бы не один белорусский ученый &mdash; профессор Петр Белькевич. Именно он в 1950-х годах открыл возможность получения из торфа воска и прочил продукту широкое применение &mdash; от косметологии до машинного производства.</p>\r\n\r\n<p style="margin-left:auto !important; margin-right:auto !important"><img alt="" src="/backend/web/assets/e073fd7/upload/images/be0fc0f1845072433efb132cf2862136.png" style="height:473px; width:800px" />В середине прошлого века Институт торфа НАН Беларуси построил в 60 километрах от Минска, возле деревни Засвятое Пуховичского района опытно-промышленную установку по получению горного, или торфяного, воска. Белькевича критиковали, называли его продукт &laquo;горе-воском&raquo;. Директора установки сменялись один за другим, а что делать с накопившимся на складе веществом, никто не знал.</p>\r\n\r\n<p style="margin-left:auto !important; margin-right:auto !important"><img alt="" src="/backend/web/assets/e073fd7/upload/images/ab751e8034802cc45f5800c1c129b2e3.png" style="height:629px; width:950px" /></p>\r\n\r\n<p style="margin-left:auto !important; margin-right:auto !important">По счастливой случайности на завод приехал представитель Всесоюзного института авиационных материалов. Ученые взяли воск на пробу для литья сверхточных изделий по выплавляемым моделям &mdash; и уже через неделю забрали всю продукцию. Оказалось, что белорусский торфяной воск, добавленный в состав пластичной массы, позволяет отливать самые точные изделия.</p>\r\n\r\n<p style="margin-left:auto !important; margin-right:auto !important"><img alt="" src="/backend/web/assets/e073fd7/upload/images/6fb2376ea8a4822bab1a957e04f59eb8.jpeg" style="height:534px; width:800px" /></p>\r\n\r\n<p style="margin-left:auto !important; margin-right:auto !important">Цветущего зеленого поселка сегодня могло и не быть, если бы не один белорусский ученый &mdash; профессор Петр Белькевич. Именно он в 1950-х годах открыл возможность получения из торфа воска и прочил продукту широкое применение &mdash; от косметологии до машинного производства.</p>\r\n', 'news_photo.jpeg', 1495642324, 1495642834, '2017-05-25', 7);

-- --------------------------------------------------------

--
-- Структура таблицы `tags`
--

CREATE TABLE IF NOT EXISTS `tags` (
  `id` int(11) NOT NULL,
  `name` text
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tags`
--

INSERT INTO `tags` (`id`, `name`) VALUES
(1, 'Люди'),
(2, 'Авто'),
(3, 'Технологии'),
(4, 'Недвижимость');

-- --------------------------------------------------------

--
-- Структура таблицы `tags_info`
--

CREATE TABLE IF NOT EXISTS `tags_info` (
  `id` int(11) NOT NULL,
  `name` text
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tags_info`
--

INSERT INTO `tags_info` (`id`, `name`) VALUES
(1, 'Новинка'),
(2, 'Авто'),
(3, 'Погода'),
(4, 'Люди'),
(5, 'Дом'),
(6, 'Город'),
(7, 'Мнения'),
(8, '2017');

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`) VALUES
(1, 'admin', '', '$2y$13$WX0IQ.4HPi3uJO.ReX4H/.qBHwTtwB1vgm3BH8huhPwr/nw84X7gW', NULL, 'admin@mail.ru', 10, 0, 0),
(2, 'qwerty', 'wDI7LdfVtP42U-apzi7--dramJgsqgRG', '$2y$13$xoq2FdSd.r.A25kxllLNaem6nBQVaqRgguuAlukrRhGHtEzWBFi3G', NULL, 'adminii@mail.ru', 10, 1495284244, 1495284244);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_id_news` (`id_news`);

--
-- Индексы таблицы `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`),
  ADD KEY `news_id_tags` (`id_tags`),
  ADD KEY `newsid_tag` (`id_tag`);

--
-- Индексы таблицы `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tags_info`
--
ALTER TABLE `tags_info`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT для таблицы `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT для таблицы `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `tags_info`
--
ALTER TABLE `tags_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_id_news` FOREIGN KEY (`id_news`) REFERENCES `news` (`id`) ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `news`
--
ALTER TABLE `news`
  ADD CONSTRAINT `news_id_tags` FOREIGN KEY (`id_tags`) REFERENCES `tags` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `newsid_tag` FOREIGN KEY (`id_tag`) REFERENCES `tags_info` (`id`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
