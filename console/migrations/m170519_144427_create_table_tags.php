<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170519_144427_create_table_tags extends Migration
{
    protected $tTags = 'tags';

    public function safeUp()
    {
        $this->createTable($this->tTags, [
            'id' => $this->primaryKey(),
            'name' => $this->text(),

        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci');
        $this->batchInsert($this->tTags, ['id', 'name'],
            [[1, 'Люди'],
                [2, 'Авто'],
                [3, 'Технологии'],
                [4, 'Недвижимость'],
            ]);


    }

    public function safeDown()
    {
        $this->dropTable($this->tTags);

    }
}


