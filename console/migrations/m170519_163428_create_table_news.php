<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170519_163428_create_table_news extends Migration
{
    protected $tNews = 'news';

    public function safeUp()
    {
        $this->createTable($this->tNews, [
            'id' => $this->primaryKey(),
            'id_tags' => $this->integer()->notNull(),
            'header' => $this->string()->notNull(),
            'text' => $this->text()->notNull(),
            'photo_src' => $this->string()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),


        ]);
        $this->addForeignKey($this->tNews . '_id_tags', $this->tNews, 'id_tags', 'tags', 'id', 'RESTRICT', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropTable($this->tNews);
    }
}


