<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170521_135619_add_column_tag_for_news extends Migration
{
    protected $tNews='news';
    public function safeUp()
    {
        $this->addColumn($this->tNews, 'id_tag', $this->integer()->null());
        $this->addForeignKey($this->tNews . 'id_tag', $this->tNews, 'id_tag', 'tags_info', 'id', 'RESTRICT', 'CASCADE');
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tNews,'id_tag');
    }
}


