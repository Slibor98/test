<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170521_090201_create_table_tags_info extends Migration
{
    protected $tTagsInfo = 'tags_info';

    public function safeUp()
    {
        $this->createTable($this->tTagsInfo, [
            'id' => $this->primaryKey(),
            'name' => $this->text(),

        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci');
        $this->batchInsert($this->tTagsInfo, ['id', 'name'],
            [[1, 'Новинка'],
                [2, 'Авто'],
                [3, 'Погода'],
                [4, 'Люди'],
                [5, 'Дом'],
                [6, 'Город'],
                [7, 'Мнения'],
                [8, '2017'],
            ]);

    }

    public function safeDown()
    {
        $this->dropTable($this->tTagsInfo);

    }
}


