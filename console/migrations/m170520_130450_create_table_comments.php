<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170520_130450_create_table_comments extends Migration
{
    protected $tComments = 'comments';
    public function safeUp()
    {
       $this->createTable($this->tComments, [
            'id' => $this->primaryKey(),
            'id_news' => $this->integer()->notNull(),
            'sender' => $this->string()->notNull(),
            'comment' => $this->text()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),

        ]);
        $this->addForeignKey($this->tComments . '_id_news', $this->tComments, 'id_news', 'news', 'id', 'RESTRICT', 'CASCADE');
    }
    
    public function safeDown()
    {
        $this->dropTable($this->tComments);

    }
}


