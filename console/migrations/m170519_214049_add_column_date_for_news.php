<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170519_214049_add_column_date_for_news extends Migration
{
    protected $tNews='news';
    public function safeUp()
    {
        $this->addColumn($this->tNews, 'date', $this->date()->null());

    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tNews,'date');

    }
}


