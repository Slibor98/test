<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170519_141910_add_user extends Migration
{
    public function safeUp()
    {
        $this->batchInsert('{{%user}}', ['id', 'username', 'password_hash', 'email',],
            [[1, 'admin', Yii::$app->security->generatePasswordHash('admin'), 'admin@mail.ru'],
            ]);
    }
    
    public function safeDown()
    {
        $this->delete('{{%user}}', ['in', 'id', 1]);
    }
}


