<?php

namespace frontend\controllers;

use Yii;
use common\models\News;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Comments;
use yii\web\Response;
use common\models\User;
use yii\data\Pagination;
use common\models\TagsInfo;


/**
 * NewsController implements the CRUD actions for News model.
 */
class NewsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all News models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => News::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single News model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $comments=Comments::find()->andWhere(['id_news' => $id])->orderBy('created_at DESC')->all();
        return $this->render('view', [
            'model' => $this->findModel($id),
            'comments'=>$comments,
        ]);
    }

    /**
     * Creates a new News model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new News();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing News model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing News model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the News model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return News the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = News::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionGetType($idTags)
    {
        $types = News::find()->andWhere(['and',
            ['id_tags' => $idTags],
            ['<=', 'date', date("Y-m-d")],
        ]);
        $tagsInfo = TagsInfo::find()->all();
        $pagination = new Pagination(['defaultPageSize' => 3, 'totalCount' => $types->count(),]);
        $pageNumber = Yii::$app->request->get('page') ? Yii::$app->request->get('page') : 1;
        return $this->render('index', [
            'types' => $types->offset(($pageNumber - 1) * 3)->limit(3)->all(),
            'pagination' => $pagination,
            'tagsInfo'=>$tagsInfo,
        ]);
    }

    public function actionGetTags($idTag)
    {
        $types = News::find()->andWhere(['and',
            ['id_tag' => $idTag],
            ['<=', 'date', date("Y-m-d")],
        ]);
        $tagsInfo = TagsInfo::find()->all();
        $pagination = new Pagination(['defaultPageSize' => 3, 'totalCount' => $types->count(),]);
        $pageNumber = Yii::$app->request->get('page') ? Yii::$app->request->get('page') : 1;
        return $this->render('index', [
            'types' => $types->offset(($pageNumber - 1) * 3)->limit(3)->all(),
            'pagination' => $pagination,
            'tagsInfo'=>$tagsInfo,
        ]);
    }

    public function actionAddFormComment()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        if (Yii::$app->request->post('commentar') !== null) {
            if (!Yii::$app->user->isGuest) {
                $comment = new Comments();
                $comment->comment = Yii::$app->request->post('commentar');
                $comment->id_news = (int)Yii::$app->request->post('id');
                $comment->sender = Yii::$app->user->identity->username;
                if ($comment->save()) {

                    return $this->renderPartial('form_comment', [
                        'comment' => $comment,
                    ]);
                }
            }
            return $this->redirect('/site/login');

        }
            return false;

    }
}
