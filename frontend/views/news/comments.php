<?php
/**
 * Created by PhpStorm.
 * User: Диана
 * Date: 20.05.2017
 * Time: 16:09
 */
/* @var $comments common\models\Comments[] */
use common\models\Comments;
?>
<div class="all-comments">
    <?php if ($comments!==0): ?>
    <?php foreach ($comments as $comment): ?>
        <li data-content="television" class="selected">
            <?= $this->render('form_comment', [
                'comment' => $comment,
            ]); ?>
        </li>
    <?php endforeach; ?>
    <?php endif; ?>
    <div class="form-comment" >
        <div class="form-group">
            <textarea class="form-control" rows="5" id="comment" style="margin-left: -30px;"></textarea>
        </div>
        <a href="#comment-product">
            <button type="button" class="btn btn-danger btn btn-primary btn-lg" id="add_comment"
                    style="background: #EF5F21">Добавить
            </button>
        </a>
    </div>
</div>

