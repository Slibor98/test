<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\components\helpers\ImageHelper;

/* @var $this yii\web\View */
/* @var $model common\models\News */
/* @var $comments common\models\Comments[] */

$this->title = $model->header;
$this->params['breadcrumbs'][] = ['label' => 'News', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<!--content-->
    <div class="blog">
        <div class="container">
            <div class="blog-top">
                <div class=" grid_3 grid-1">
                    <h3 style="text-align: center;"><a href="blog_single.html"><?= $model->header; ?></a></h3>
                    <div class="col-md-12" style="text-align: center;">
                    <?= Html::a(ImageHelper::getThumb($model->getUploadDirectory() . DIRECTORY_SEPARATOR . $model->photo_src,[1140,350], ['class' => 'img-responsive',
                        'style'=>'margin: auto']),
                        \yii\helpers\Url::to(['news/view', 'id'=>$model->id])) ?>
                        </div>
                    <?= Html::hiddenInput(null, $model->id, [
                        'id' => 'hidden_news-id',
                    ]) ?>
                    <?= Html::hiddenInput(null, Yii::$app->user->identity->id, [
                        'id' => 'hidden_user-id',
                    ]) ?>
                    <div class="blog-poast-info">
                        <ul>
                            <li><a class="admin" href="#"><i> </i> Admin </a></li>
                            <li><span><i class="date"> </i> <?= date(\common\components\date\DateHelper::FORMAT_DATE, $model->created_at)?></span></li>
                                <li><a class="p-blog" href="#"><i class="comment"></i><?= $model->getComments()->count()?></a></li>
                        </ul>
                    </div>
                    <div class="col-md-12" style="text-align: center;">
                  <?= $model->text;?>

                        </div>
                    <ul class="cd-tabs-content">
                        <?= $this->render('comments', ['comments' => $comments]) ?>
                        <div class="clearfix"></div>

                    </ul>
                </div>

            </div>


        </div>
    </div>
