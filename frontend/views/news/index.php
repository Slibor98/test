<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use common\components\helpers\ImageHelper;
use yii\widgets\LinkPager;
use yii\data\Pagination;
use common\models\TagsInfo;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $types common\models\News[] */
/* @var $pagination Pagination */
/* @var  $tagsInfo TagsInfo[] */

$this->title = 'Новости';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-md-12">
    <div class="col-md-3 product-price" style="margin-left: 90px;">
        <div class="sellers">
            <div class="of-left-in">
                <h3 class="tag">Теги</h3>
            </div>
            <div class="tags">
                <ul>
                    <?php foreach ($tagsInfo as $tag): ?>
                        <li><?= Html::a($tag->name, Url::to(['news/get-tags', 'idTag' => $tag->id])) ?></li>
                    <? endforeach; ?>
                    <div class="clearfix"></div>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-md-8" style="padding-top: 50px;text-align: center;padding-right: 100px;">
        <h1><?= Html::encode($this->title); ?></h1>
    </div>
</div>
<div class="blog">
    <div class="container">
        <div class="blog-top">
            <?php if ($types!==null): ?>
            <?php foreach ($types as $type): ?>
                <div class="col-md-4 grid_3">
                    <h3> <?= Html::a($type->header, Url::to([
                            'news/view', 'id' => $type->id,
                        ])) ?></h3>
                    <?= Html::a(ImageHelper::getThumb($type->getUploadDirectory() . DIRECTORY_SEPARATOR . $type->photo_src,
                        [270, 270], ['class' => 'img-responsive']),
                        Url::to([
                            'news/view', 'id' => $type->id,
                        ])) ?>

                    <div class="blog-poast-info">
                        <ul>
                            <li><span><i class="date"> </i>
                                    <?= date(\common\components\date\DateHelper::FORMAT_DATE, $type->created_at) ?>
                                </span></li>
                            <li><a class="p-blog" href="#"><i class="comment"> </i><?= $type->getComments()->count()?></a></li>
                        </ul>
                    </div>
                    <div
                        class="button"> <?= Html::a('Узнать больше', \yii\helpers\Url::to(['news/view', 'id' => $type->id])) ?></div>
                </div>
            <?php endforeach; ?>
            <?php endif; ?>
            <div class="clearfix"></div>
        </div>
        <div class="col-md-12" style="text-align: center;">
            <?= LinkPager::widget(['pagination' => $pagination]); ?>
        </div>

    </div>
</div>
<!--//content-