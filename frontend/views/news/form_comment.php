<?php
/**
 * Created by PhpStorm.
 * User: Диана
 * Date: 15.03.2017
 * Time: 11:19
 */
/* @var $comment common\models\Comments */
use yii\helpers\Html;
?>
<div class="comments-top-top" id="citata-all">
    <div class="top-comment-left">
        <img class="img-responsive" src="/images/co.png" alt="">
    </div>
    <div class="top-comment-right" style="margin: 0px -30px;">
        <h6><a href="#"><?=$comment->sender; ?></a> - <?= date(\common\components\date\DateHelper::FORMAT_DATE, $comment->created_at); ?></h6>
        <div> <p><?= $comment->comment; ?></p></div>
        <div class="citata" style="display: none;">
            <a href="#" id="cit" style="float: right;">Цитировать</a> </div>
    </div>
    <div class="clearfix"></div>
</div>