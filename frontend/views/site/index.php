<?php

use yii\bootstrap\Html;
use common\models\News;
use common\components\helpers\ImageHelper;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\data\Pagination;
use common\models\TagsInfo;

/* @var $this yii\web\View */
/* @var $news common\models\News[] */
/* @var $pagination Pagination */
/* @var  $tagsInfo TagsInfo[] */

$this->title = 'Новости';

?>

<div class="col-md-12">
    <div class="col-md-3 product-price" style="margin-left: 90px;">
        <div class="sellers">
            <div class="of-left-in">
                <h3 class="tag">Теги</h3>
            </div>
            <div class="tags">
                <ul>
                    <?php foreach ($tagsInfo as $tag): ?>
                        <li><?= Html::a($tag->name, Url::to(['news/get-tags', 'idTag' => $tag->id])) ?></li>
                    <? endforeach; ?>
                    <div class="clearfix"></div>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-md-8" style="padding-top: 50px;text-align: center;padding-right: 100px;">
        <h1><?= Html::encode($this->title); ?></h1>
    </div>
</div>
<div class="blog">
    <div class="container">
        <div class="blog-top">
            <?php if ($news !== null): ?>
                <?php foreach ($news as $oneNews): ?>
                    <div class="col-md-4 grid_3">
                        <h3> <?= Html::a($oneNews->header, Url::to([
                                'news/view', 'id' => $oneNews->id,
                            ])) ?></h3>
                        <?= Html::a(ImageHelper::getThumb($oneNews->getUploadDirectory() . DIRECTORY_SEPARATOR . $oneNews->photo_src,
                            [270, 270], ['class' => 'img-responsive']),
                            Url::to([
                                'news/view', 'id' => $oneNews->id,
                            ])) ?>

                        <div class="blog-poast-info">
                            <ul>
                                <li><span><i class="date"> </i>
                                        <?= $oneNews->date; ?>
                                </span></li>
                                <li><a class="p-blog" href="#"><i class="comment"> </i><?= $oneNews->getComments()->count()?></a></li>
                            </ul>
                        </div>
                        <div
                            class="button"> <?= Html::a('Узнать больше', \yii\helpers\Url::to(['news/view', 'id' => $oneNews->id])) ?></div>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
            <div class="clearfix"></div>
        </div>
        <div class="col-md-12" style="text-align: center;">
            <?= LinkPager::widget(['pagination' => $pagination]); ?>
        </div>

    </div>
</div>
<!--//content-->