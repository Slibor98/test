<?php

/* @var $this \yii\web\View */
/* @var $content string */
/* @var $tag common\models\Tags */

use yii\helpers\Html;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use common\models\Tags;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <!--//fonts-->
    <!-- start menu -->
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<!--header-->
<div class="header">
    <div class="header-top">
        <div class="container">
            <div class="header-left">
                <?php if (Yii::$app->user->isGuest): ?>

                    <ul>
                        <li>
                            <div class="b-top-profile__user-account"
                                 data-bind="visible: $root.currentUser.id()">
                                <!-- ko if: $root.currentUser.id() -->
                                <!-- /ko -->

                                <p class="user-name">

                                </p>
                                <ul class="user-bar cfix ">
                                    <li>

                                        <?= Html::a('Войти', \yii\helpers\Url::to(['site/login'])) ?>

                                    </li>
                                    <li>  <?= Html::a('Регистрация', \yii\helpers\Url::to(['site/signup'])) ?></li>

                                </ul>
                            </div>
                        </li>


                    </ul>
                    <div class="clearfix"></div>
                <?php else: ?>

                    <ul>
                        <li>
                            <div class="b-top-profile__user-account"
                                 data-bind="visible: $root.currentUser.id()">
                                <!-- ko if: $root.currentUser.id() -->
                                <!-- /ko -->
                                <ul class="user-bar cfix ">
                                    <li>
                                        <?= Html::a('Выйти', \yii\helpers\Url::to(['site/logout']),
                                            ['class' => 'profile',
                                                'data-method' => 'post']); ?>
                                    </li>

                                </ul>
                            </div>
                        </li>


                    </ul>
                    <div class="clearfix"></div>
                <?php endif; ?>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="container">
        <div class="head-top">
            <div class="logo" style="    padding-left: 15px;">
                <?= Html::a('<img src="/images/n.png" alt="">',\yii\helpers\Url::to(['site/index'])) ?>

            </div>
            <div class=" h_menu4">
                <ul class="memenu skyblue">
                    <li class="active grid">
                        <?= Html::a('<b>' . 'Главная' . '</b>', Url::to(['site/index']), ['class' => 'color8']) ?>
                    </li>
                    <?php foreach (Tags::find()->all() as $tag): ?>
                        <li>
                            <?= Html::a('<b>' . $tag->name . '</b>', Url::to(['news/get-type', 'idTags' => $tag->id]), [
                                'class' => 'color1',
                            ]) ?>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>

            <div class="clearfix"></div>
        </div>
    </div>

</div>
<?= Alert::widget() ?>
<?= $content; ?>
<!--footer-->

<div class="footer-class">
    <p>© 2017 News Store | Design by Sliborskaya Diana </p>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
