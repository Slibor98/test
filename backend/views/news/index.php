<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\News;
use common\components\date\DateHelper;
use common\models\Tags;
use common\models\TagsInfo;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Новости';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Создать новость', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget(['dataProvider' => $dataProvider,
    'columns' => [['attribute' => 'id',
        'label' => '№',
    ],
    [
        'attribute' => 'id_tags',
        'label' => 'Категория',
        'value' => function (\common\models\News $model) {
            return Tags::findOne([['id' => $model->id_tags]])->name;
        }
    ],
    ['attribute' => 'header',
    'label' => 'Заголовок',
    ],
    [
        'attribute' => 'date',
        'label' => 'Дата публикации',
        'format' => 'raw',
        'value' => function (News $model) {
            return $model->date ?
                DateHelper::format($model->date, DateHelper::FORMAT_DATE, DateHelper::FORMAT_USER_DATE) :
                'Не указана';
        }
    ],
        [
            'attribute' => 'id_tag',
            'label' => 'Тег',
            'format' => 'raw',
            'value' => function (News $model) {
                return $model->id_tag ?
                    TagsInfo::findOne([['id' => $model->id_tag]])->name :
                    'Не указан';
            }
        ],
            ['class' => 'yii\grid\ActionColumn'],
        ],

    ]); ?>
</div>
