<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\News;
use common\components\date\DateHelper;
use common\models\TagsInfo;

/* @var $this yii\web\View */
/* @var $model common\models\News */

$this->title = $model->header;

?>
<div class="news-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'id_tags',
                'value' => function (\common\models\News $model) {
                    return \common\models\Tags::findOne([['id' => $model->id_tags]])->name;

                },
            ],
            [
                'attribute' => 'header',
                'format' => 'raw',
                'value' => function (\common\models\News $model) {
                    return $model->header;
                },
            ],
            [
                'attribute' => 'text',
                'format' => 'raw',
                'value' => function (\common\models\News $model) {
                    return $model->text;
                },
            ],
            ['attribute' => 'photo_src',
                'format' => 'raw',
                'value' => function (\common\models\News $model) {
                    return $model->photo_src;
                }
            ],
            [
                'attribute' => 'date',
                'label' => 'Дата публикации',
                'format' => 'raw',
                'value' => function (News $model) {
                    return $model->date ?
                        DateHelper::format($model->date, DateHelper::FORMAT_DATE, DateHelper::FORMAT_USER_DATE) :
                        'Не указана';
                }
            ],
            [
                'attribute' => 'id_tag',
                'label' => 'Тег',
                'format' => 'raw',
                'value' => function (News $model) {
                    return $model->id_tag ?
                        TagsInfo::findOne([['id' => $model->id_tag]])->name :
                        'Не указан';
                }
            ],

        ],
    ]) ?>

</div>
