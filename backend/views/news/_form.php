<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Tags;
use dosamigos\ckeditor\CKEditor;
use common\components\date\DateHelper;
use nex\datepicker\DatePicker;
use common\models\TagsInfo;

/* @var $this yii\web\View */
/* @var $model common\models\News */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="news-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_tags')->dropDownList(ArrayHelper::map(Tags::find()->all(), 'id', 'name'), ['prompt' => 'Выберите']) ?>

    <?= $form->field($model, 'header')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'text')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'standart',
    ]) ?>

    <?= $form->field($model, 'photo_src')->fileInput() ?>

    <div style="width: 210px;margin-bottom: 10px;">
        <p><b>Дата публикации</b></p>
        <?= DatePicker::widget([
            'model' => $model,
            'attribute' => 'date',
            'language' => 'ru',
            'size' => 'lg',
            'readonly' => true,
            'placeholder' => 'Выберите дату',
            'clientOptions' => [
                'format' => 'L',
                'minDate' => date(\common\components\date\DateHelper::FORMAT_DATE),
                'maxDate' => '2099-01-01',
            ],
            'clientEvents' => [
                'dp.show' => new \yii\web\JsExpression("function () { console.log('It works!'); }"),
            ],
        ]); ?> </div>
    <?= $form->field($model, 'id_tag')->dropDownList(ArrayHelper::map(TagsInfo::find()->all(), 'id', 'name'), ['prompt' => 'Выберите']) ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
