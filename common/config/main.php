<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
        ],
    //    'assetManager' => [
    //        'bundles' => [
    //            'yii\bootstrap\BootstrapPluginAsset' => [
    //                'js' => [],
    //            ],
    //        ],
    //        'appendTimestamp' => true,
    //    ],
    ],
    'language' => 'ru-RU',
    'sourceLanguage' => 'ru-RU',
];
