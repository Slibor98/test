<?php

/**
 * Created by PhpStorm.
 * User: Диана
 * Date: 10.02.2017
 * Time: 14:00
 */
namespace common\assets;


use yii\web\AssetBundle;

class CommonAsset extends AssetBundle
{
    public $sourcePath = '@common/assets/web';

    public $css = [
        'css/bootstrap.css',
        'css/flexslider.css',
        'css/memenu.css',
        'css/style.css'

    ];

    public $js = [
        // 'js/jquery.min.js',
        'js/jquery.flexslider.js',
        'js/main.js',
        'js/memenu.js',
        'js/responsiveslides.min.js',
        'js/simpleCart.min.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}