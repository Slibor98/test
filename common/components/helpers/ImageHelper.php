<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 07.04.2016
 * Time: 15:11
 */

namespace common\components\helpers;

use common\components\image\EasyThumbnailImage;
use common\components\helpers\ArrayHelper;
use Yii;

/**
 * Class ImageHelper
 * @package common\components
 */
class ImageHelper
{
    /**
     *
     */
    const THUMB_50_50 = 'thumb_50_50';
    /**
     *
     */
    const THUMB_SMALL = 'thumb_small';
    /**
     *
     */
    const THUMB_100_100 = 'thumb_100_100';
    /**
     *
     */
    const THUMB_150_150 = 'thumb_150_150';
    /**
     *
     */
    const THUMB_MEDIUM = 'thumb_medium';
    /**
     *
     */
    const THUMB_LARGE = 'thumb_large';
    /**
     *
     */
    const PROFILE_SIZE = [500, 500];

    /**
     *
     */
    const DEFAULT_THUMB = self::THUMB_MEDIUM;

    /**
     * @var array
     */
    static $thumbSizes = [
        self::THUMB_50_50 => [50, 50],
        self::THUMB_SMALL => [100, 100],
        self::THUMB_100_100 => [100, 100],
        self::THUMB_150_150 => [150, 150],
        self::THUMB_MEDIUM => [200, 200],
        self::THUMB_LARGE => [600, 600],
    ];

    /**
     * @param $filePath
     * @param string $thumb
     * @param array $options
     * @return string
     */
    public static function getThumb($filePath, $thumb = self::DEFAULT_THUMB, $options = [])
    {
        $thumbSize = self::getThumbSizes($thumb);
        $cutType = ArrayHelper::remove($options, 'cutType', EasyThumbnailImage::THUMBNAIL_OUTBOUND);

        if (!is_file($filePath) || !file_exists($filePath)) {
            $filePath = Yii::getAlias('@webroot') . Yii::$app->params['image.placeholder'];
        }

        return EasyThumbnailImage::thumbnailImg(
            $filePath,
            $thumbSize[0], $thumbSize[1],
            $cutType,
            $options
        );
    }

    /**
     * @param $filePath
     * @param string $thumb
     * @param array $options
     * @return string
     */
    public static function getThumbSrc($filePath, $thumb = self::DEFAULT_THUMB, $options = [])
    {
        $thumbSize = self::getThumbSizes($thumb);

        if (!is_file($filePath) || !file_exists($filePath)) {
            $filePath = Yii::getAlias('@webroot') . Yii::$app->params['image.placeholder'];
        }

        return EasyThumbnailImage::thumbnailSrc(
            $filePath,
            $thumbSize[0], $thumbSize[1],
            EasyThumbnailImage::THUMBNAIL_OUTBOUND,
            $options
        );
    }

    /**
     * @param string|array $thumb
     * @return array
     */
    public static function getThumbSizes($thumb = self::DEFAULT_THUMB)
    {
        $thumbSizes = self::$thumbSizes[self::DEFAULT_THUMB];

        if (is_array($thumb)) {
            if (isset($thumb[0]) && isset($thumb[1])) {
                $thumbSizes = [$thumb[0], $thumb[1]];
            }
        } elseif (isset(self::$thumbSizes[$thumb])) {
            $thumbSizes = self::$thumbSizes[$thumb];
        }

        return $thumbSizes;
    }

    /**
     * @param $filePath
     * @return int|null
     */
    public static function getModificationDate($filePath)
    {
        $date = null;
        if (file_exists($filePath) && is_readable($filePath) && is_file($filePath)) {
            $date = filemtime($filePath);
        }

        return $date;
    }
}
