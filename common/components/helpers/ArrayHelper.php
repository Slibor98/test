<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 07.04.2016
 * Time: 15:09
 */

namespace common\components\helpers;


/**
 * Class ArrayHelper
 * @package common\components\helpers
 */
class ArrayHelper extends \yii\helpers\ArrayHelper
{
    /**
     * @param array $array
     * @param $key
     * @param null $group
     * @return array
     */
    public static function getAssoc($array, $key, $group = null)
    {
        return static::map($array, $key, function ($model) {
            return $model;
        }, $group);
    }
}