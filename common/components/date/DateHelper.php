<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 19.02.2016
 * Time: 16:11
 */

namespace common\components\date;

/**
 * Class DateHelper
 * @package common\components\date
 */
/**
 * Class DateHelper
 * @package common\components\date
 */
class DateHelper
{

    /**
     * Date in DATE mysql column
     */
    const FORMAT_DATE = 'Y-m-d';
    /**
     * Date in DATETIME mysql column
     */
    const FORMAT_DATETIME = 'Y-m-d H:i:s';
    /**
     * Date in DatePicker js plugin
     */
    const FORMAT_USER_DATE = 'd.m.Y';
    /**
     *
     */
    const FORMAT_USER_DATE_SHORT = 'd.m.y';
    /**
     *
     */
    const FORMAT_USER_DAY_MONTH = 'd.m';
    /**
     *
     */
    const FORMAT_USER_DATETIME = 'd.m.Y H:i:s';
    /**
     *
     */
    const FORMAT_SUER_DATETIME_WITHOUT_SECONDS = 'd.m.Y H:i';

    /**
     *
     */
    const MIN_DATE = '1900-01-01';
    /**
     *
     */
    const MIN_USER_DATE = '01.01.1900';

    /**
     * Date intervals for contracts filter
     */
    const TODAY = 'today';
    /**
     *
     */
    const YESTERDAY = 'yesterday';
    /**
     *
     */
    const FOR_2_DAYS = 'for 2 days';
    /**
     *
     */
    const All = 'custom period';

    /**
     * @var array
     */
    public static $dateIntervals = [
        self::All => 'Все',
        self::TODAY => 'Сегодня',
        self::YESTERDAY => 'Вчера',
        self::FOR_2_DAYS => 'За 2 дня',
    ];

    /**
     * @param $date
     *
     * @return string
     */
    public static function getCorrectDateFromDatepicker($date)
    {
        return self::format($date, self::FORMAT_DATE, self::FORMAT_USER_DATE);
    }

    /**
     * @return bool|string
     */
    public static function getCreatedAtDate()
    {
        return date(self::FORMAT_DATE);
    }

    /**
     * Formats date
     *
     * @param string $inDate
     * @param string $outFormat
     * @param string $inFormat
     *
     * @return string
     */
    public static function format($inDate, $outFormat, $inFormat = self::FORMAT_DATETIME)
    {
        if ($inDate == '0000-00-00') {
            return false;
        }

        if (self::validateDate($inDate, $outFormat)) {
            return $inDate;
        }

        $date = \DateTime::createFromFormat($inFormat, $inDate);

        return $date
            ? $date->format($outFormat)
            : false;
    }

    /**
     * Validates date for format
     * @param $date
     * @param string $inFormat
     * @return bool
     */
    public static function validateDate($date, $inFormat = self::FORMAT_DATE)
    {
        $d = \DateTime::createFromFormat($inFormat, $date);

        return $d && $d->format($inFormat) == $date;
    }

    /**
     * @param $interval
     *
     * @return array
     */
    public static function getDateInterval($interval)
    {
        switch ($interval) {
            case self::TODAY :
                return [
                    'dateFrom' => date(self::FORMAT_USER_DATE),
                    'dateTo' => date(self::FORMAT_USER_DATE),
                ];
                break;
            case self::YESTERDAY :
                return [
                    'dateFrom' => date(self::FORMAT_USER_DATE, strtotime('-1 day')),
                    'dateTo' => date(self::FORMAT_USER_DATE, strtotime('-1 day')),
                ];
                break;
            case self::FOR_2_DAYS:
                return [
                    'dateFrom' => date(self::FORMAT_USER_DATE, strtotime('-2 day')),
                    'dateTo' => date(self::FORMAT_USER_DATE),
                ];
                break;
            default:
                return [
                    'dateFrom' => null,
                    'dateTo' => null,
                ];
        }
    }

    /**
     * @param $interval array
     * @param $dateCheckFrom string
     * @param $dateCheckTo string
     *
     * @return bool
     */
    public static function checkDateInInterval($interval, $dateCheckFrom, $dateCheckTo)
    {
        $dateInterval = self::getDateInterval($interval);

        if ($dateInterval['dateFrom'] == $dateCheckFrom && $dateInterval['dateTo'] == $dateCheckTo) {
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     */
    const INTERVAL_TODAY = 1;
    /**
     *
     */
    const INTERVAL_YESTERDAY = 2;
    /**
     *
     */
    const INTERVAL_WEEK = 3;
    /**
     *
     */
    const INTERVAL_MONTH = 4;
    /**
     *
     */
    const INTERVAL_QUARTER = 5;

    /**
     *
     */
    const INTERVAL_RETURN_DATETIME = 1;
    /**
     *
     */
    const INTERVAL_RETURN_STRING = 2;
    /**
     *
     */
    const INTERVAL_RETURN_TIMESTAMP = 3;

    /**
     * @param $interval
     * @param int $returnType String, Timestamp or DateTime
     *
     * @return array
     */
    public static function getDateIntervalAlt($interval, $returnType = self::INTERVAL_RETURN_DATETIME)
    {
        $dateTo = new \DateTime();
        $dateFrom = null;
        switch ($interval) {
            case self::INTERVAL_TODAY:
                $dateFrom = new \DateTime();
                break;
            case self::INTERVAL_YESTERDAY:
                $dateFrom = $dateTo = new \DateTime('-1 day');
                break;
            case self::INTERVAL_WEEK:
                $dateFrom = new \DateTime('first day of this week');
                break;
            case self::INTERVAL_MONTH:
                $dateFrom = new \DateTime('first day of this month');
                break;
            case self::INTERVAL_QUARTER:
                $dateFrom = self::getCurrentQuarterStart();
                break;
        }

        return self::_returnDateInterval($dateFrom, $dateTo, $returnType);
    }

    /**
     * @param \DateTime $dateFrom
     * @param \DateTime $dateTo
     * @param $returnType
     * @return array
     */
    private static function _returnDateInterval(\DateTime $dateFrom, \DateTime $dateTo, $returnType)
    {
        switch ($returnType) {
            case self::INTERVAL_RETURN_DATETIME:
                return [
                    $dateFrom,
                    $dateTo,
                ];
            case self::INTERVAL_RETURN_STRING:
                return [
                    $dateFrom->format(DateHelper::FORMAT_DATE),
                    $dateTo->format(DateHelper::FORMAT_DATE),
                ];
            case self::INTERVAL_RETURN_TIMESTAMP:
                return [
                    $dateFrom->getTimestamp(),
                    $dateTo->getTimestamp(),
                ];
            default:
                throw new \BadFunctionCallException();
        }
    }

    /**
     * Returns first day of current quarter.
     * @return \DateTime
     */
    public static function getCurrentQuarterStart()
    {
        $quarterNumber = self::getCurrentQuarterNumber();

        $startQuarterMonth = ($quarterNumber - 1) * 3;

        return new \DateTime(date('Y') . '-' . $startQuarterMonth); // creates date: 2015-3 to 2015-03-01
    }

    /**
     * @return int
     */
    public static function getCurrentQuarterNumber()
    {
        $month = date('n'); // month without leading zero

        return ((int)(($month - 1) / 3)) + 1;
    }

}