<?php

namespace common\models;

use common\components\date\DateHelper;
use common\components\date\DatePickerFormatBehavior;
use Yii;
use yii\helpers\ArrayHelper;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Html;
use yii\web\UploadedFile;

/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property integer $id_tags
 * @property integer $id_tag
 * @property string $header
 * @property string $text
 * @property string $photo_src
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $date
 * @property Tags $idTags
 * @property TagsInfo $idTag
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    const PHOTO_NAME = 'news_photo';
    public $photo;
    public static function tableName()
    {
        return 'news';
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            [
                'class' => DatePickerFormatBehavior::className(),
                'attributes' => [
                    'date' => [
                        'message' => 'Дата указана неверно.',
                    ],
                ],
                'dateFormatParams' => [
                    'whenClient' => 'function(){}',
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_tags', 'header', 'text'], 'required'],
            [['id_tags','id_tag', 'created_at', 'updated_at'], 'integer'],
            [['text'], 'string'],
            [['date'], 'safe'],
            [['header'], 'string', 'max' => 255],
            [['id_tags'], 'exist', 'skipOnError' => true, 'targetClass' => Tags::className(), 'targetAttribute' => ['id_tags' => 'id']],
            [['id_tag'], 'exist', 'skipOnError' => true, 'targetClass' => TagsInfo::className(), 'targetAttribute' => ['id_tag' => 'id']],
        ];
    }
    public function getUploadDirectory()
    {
        $filename = Yii::getAlias('@common/uploads/news');
        if (!file_exists($filename . DIRECTORY_SEPARATOR . $this->id)) {
            mkdir($filename . DIRECTORY_SEPARATOR . $this->id);
        }
        return $filename . DIRECTORY_SEPARATOR . $this->id;
    }

    public function _prepare()
    {
        $this->photo = UploadedFile::getInstance($this, 'photo_src');
        if ($this->photo != null) {

            return true;
        }
        return false;

    }

    public function _savePhoto()
    {
        $this->photo->saveAs($this->getUploadDirectory() . DIRECTORY_SEPARATOR . self::PHOTO_NAME . '.' . $this->photo->extension);
    }

    /**
     * @inheritdoc
     */
    public function _save()
    {

        if ($this->save()) {
            if ($this->_prepare()) {
                $this->_savePhoto();
                $this->photo_src = self::PHOTO_NAME . '.' . $this->photo->extension;

                return $this->save(false, ['photo_src']);
            }
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '№',
            'id_tags' => 'Категория',
            'header' => 'Заголовок',
            'text' => 'Описание',
            'photo_src' => 'Изображение',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'date'=>'Дата',
            'id_tag'=>'Тег',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdTags()
    {
        return $this->hasOne(Tags::className(), ['id' => 'id_tags']);
    }
    public function getIdTag()
    {
        return $this->hasOne(TagsInfo::className(), ['id' => 'id_tag']);
    }
    public function getComments()
    {
        return $this->hasMany(Comments::className(), ['id_news' => 'id']);
    }
}
