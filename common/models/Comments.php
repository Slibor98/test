<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "comments".
 *
 * @property integer $id
 * @property integer $id_news
 * @property string $sender
 * @property string $comment
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property News $idNews
 */
class Comments extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comments';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    public function rules()
    {
        return [
            [['id_news', 'sender', 'comment'], 'required'],
            [['id_news', 'created_at', 'updated_at'], 'integer'],
            [['comment'], 'string'],
            [['sender'], 'string', 'max' => 255],
            [['id_news'], 'exist', 'skipOnError' => true, 'targetClass' => News::className(), 'targetAttribute' => ['id_news' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_news' => 'Id News',
            'sender' => 'Sender',
            'comment' => 'Comment',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdNews()
    {
        return $this->hasOne(News::className(), ['id' => 'id_news']);
    }
}
