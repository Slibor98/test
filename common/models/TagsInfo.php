<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tags_info".
 *
 * @property integer $id
 * @property string $name
 */
class TagsInfo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tags_info';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '№',
            'name' => 'Наименование',
        ];
    }
}
